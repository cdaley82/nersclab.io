#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "advisor-annotate.h"  // Add to each module that contains Intel Advisor annotations

int main(int argc,char *argv[])
{
  // Define Variables
double begin, end;
double time_spent;
begin=omp_get_wtime();
int **x, *y,*A;
int n=atoi(argv[1]);
int i,j;
y=(int *)malloc(n*sizeof(int));
A=(int *)malloc(n*sizeof(int));
x=(int **)malloc(n*sizeof(int*));
for(i=0;i<n;i++)
x[i]=(int *)malloc(n*sizeof(int));
for (i=0; i<n; i++) {
    for (j=0; j<n; j++)
                   x[i][j]= i%10;
    }
for (i=0; i<n; i++) {
        y[i]= i%10;
        A[i] = 0;
  }
ANNOTATE_SITE_BEGIN( MySite1 );  // Place before the loop control statement (and before any loop directives) to begin a parallel code region (parallel site).
// loop control statement
for (i=0; i<n; i++) {
ANNOTATE_ITERATION_TASK( MyTask1 );  // Place at the start of loop body. This annotation identifies an entire body as a task. 
    // loop body
    for (j=0; j<n; j++)
      A[i] += x[i][j] * y[j];
}
ANNOTATE_SITE_END();  // End the parallel code region, after task execution completes
end=omp_get_wtime();
time_spent = (double)(end - begin);
printf("Time=%lf",time_spent);
}
