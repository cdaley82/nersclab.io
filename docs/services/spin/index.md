# Spin

## Overview

Spin is a container-based platform at NERSC designed for you to
deploy your own science gateways, workflow managers, databases,
API endpoints, and other network services to support your
scientific projects. Services in Spin are built with Docker containers
and can easily access NERSC systems and storage.

For more general information, see the
[Spin web page](https://www.nersc.gov/systems/spin/).

## Rancher 1 versus Rancher 2

Spin is built on the Rancher orchestration system. NERSC is currently
supporting two major versions of Rancher:

* **Rancher 1 (deprecated)**: Originally launched in May 2018, this
version of Rancher is based on Docker Compose technology and is
entirely driven by command-line interface and YAML markup. Support
for Rancher 1 will be provided at least until early 2021.

* **Rancher 2**: Launched in April 2020, this version of Rancher is a
major upgrade based on the popular and robust Kubernetes system. It
introduces an entirely new web user interface and command line
interface.

***All Spin users are strongly encouraged to migrate their services
from Rancher 1 to Rancher 2 as soon as possible.***

## Rancher 1 Documentation

Online documentation includes

* a three-part [Getting Started Guide](rancher1/getting_started/index.md)
* a [Cheat Sheet](rancher1/cheatsheet.md) of common commands
* a [Reference Guide](rancher1/reference_guide.md)

## Rancher 2 Documentation

***These documents are under development.***

Online documentation includes

* [Concepts and Terminology](rancher2/concepts.md)
* a [CLI Guide](rancher2/cli.md)
<!--- * a [Reference Guide](rancher2/reference_guide.md) --->
* a [Migration Guide](rancher2/migration.md) (on the Rancher web site)

## Get Started and Get Help

Instructor-led workshops and self-guided training is available for
Rancher 2. For more information, see the
[Spin Training & Tutorials page](https://www.nersc.gov/users/training/spin/).
