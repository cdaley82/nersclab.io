# Collaboration Accounts

## Overview

Most NERSC login accounts are associated with specific individuals and
must not be shared. Sometimes it is advantageous to have a login
account which is not tied to a person but instead to the group for the
purposes of shared access to batch jobs or data. Collaboration
Accounts are designed to facilitate collaborative computing by
allowing multiple users to use the same account.  All actions
performed by the collaboration account are traceable back to the
individual who used the collaboration account to perform those actions
via gsisshd accounting logs. PIs and PI Proxies can request a
collaboration account by submitting a ticket.

## Obtaining a Collaboration Account

PIs or PI Proxies can request a collaboration account for their project by opening a ticket containing

* Name of collaboration account
* Contact email for person responsible for collaboration account
* Project owning collaboration account

!!! tip
    Currently collaboration names must be limited to six characters.

## Logging Into Collaboration Accounts

To access your collaboration account on any Cori login node or on any
data transfer node (DTN), use:

```console
$ collabsu <collaboration account name>
<enter nersc password at the prompt>
```

!!! tip
    Only your NERSC password is required for collabsu. You do not need
    your one-time password.

## Controlling Collaboration Account Access

Each collaboration account has two linux file groups associated with
it, one with the same name as the collaboration account and one with
the name `c_<collaboration account>`. PIs and PI Proxies can give users
in their project access to the collaboration account by [adding them to
the `c_<collaboration account>` group in
Iris](../../iris/iris-for-pis/#add-a-user-to-a-unix-group).

## External Access and Collaboration Accounts

### Transferring Data via Globus

At the request of the PI or PI Proxy, NERSC can set up a dedicated
[Globus](../services/globus.md) endpoint that will allow data
transfers via Globus as the collaboration account. This is a valuable
tool for groups that need to make large data transfers into or out of
NERSC and would like the data to be owned by the collaboration
account. Only NERSC users who could already become the collaboration
account are able to access these endpoints. If you are interested in
this, please open a ticket and we'll work with you to set one up.

### Access via ssh

If you need to have external ssh-based access to NERSC with your
collaboration account (for instance for transferring data), you can
use [sshproxy](../../connect/mfa#sshproxy) to generate an ssh key for
the account

```console
$ sshproxy.sh -c your_collab_acct
Enter the password+OTP for elvis: 
Successfully obtained ssh key /Users/elvis/.ssh/your_collab_acct
Key /Users/elvis/.ssh/your_collab_acct is valid: from 2019-07-16T09:46:00 to 2019-07-17T09:47:58
```

You can use this key to ssh (or scp, etc.) to any NERSC system.

## Use Cases

### Collaborative Data Management

Large scale experimental and simulation data are typically read or
written by multiple collaborators and are kept on disk for long
periods. A problem that often arises is that the files are owned by
the collaborator who did the work and if that collaborator changes
roles the default unix file permissions usually are such that the
files cannot be managed (deleted) by other members of the
collaboration and system administrators must be contacted. While the
problem can be addressed with the appropriate use of unix groups and
file permissions in practice this tends to be problematic and a more
seamless solution would be of great utility.

### Collaborative Software Management

The issue with managing software is similar to that of managing data –
different collaborators often need to work with the same files in a
particular software installation and unix groups and file permissions
tend to be problematic for them.  The main difference between
collaborative data and software management is that software is
typically managed on a short-tem basis (hours/days) and smaller in
size (~GBs) whereas production data is managed on a long-term basis
(months/years) and much larger (~TBs to ~PBs).

### Collaborative Job Management

Production level jobs are often run by a small team of collaborators.
Project accounts would enable members of the team to manipulate jobs
submitted by other team members as necessary.
