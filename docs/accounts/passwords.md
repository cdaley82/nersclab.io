# Passwords

## Password and Account Protection

A user is given a username (also known as a login name) and associated
password that permits her/him to access NERSC resources.  This
username/password pair may be used by a single individual only:
*passwords must not be shared with any other person*. Users who
share their passwords will have their access to NERSC disabled.

Passwords must be changed as soon as possible after exposure or
suspected compromise.  Exposure of passwords and suspected compromises
must immediately be reported to NERSC at security@nersc.gov or the
Account Support Group, accounts@nersc.gov.

## Forgotten Passwords

If you forget your password or if it has recently expired, you can
reset your password by following the steps below.

1.  Click the 'Forgot password?' link on the [Iris login
    page](https://iris.nersc.gov).

    ![Iris: login page](../iris/images/iris_login_page.png)
    {: align="center"}

2.  Enter your username, the email address that registered for your
    NERSC account in Iris, and, if MFA has been enabled, an MFA
    One-Time Password (OTP), and click OK.

    ![Iris: Forgot your password?](images/iris_login_reset_pw_02b.png)
    {: align="center"}

3.  A dialog box asks if you are really want to reset password.
    Click 'OK'.

    ![Iris: Are you sure?](images/iris_login_reset_pw_03.png)
    {: align="center" style="border:1px solid black"}

4.  Then, another dialog box appears to tell you that an email has
    been successfully sent to you.

    ![Iris: Email sent](images/iris_login_reset_pw_04.png)
    {: align="center" style="border:1px solid black"}

5.  The email contains a link to reset your password. Click the link.
6.  The password change page contains the Secret Code field pre-filled.
    The code will be valid for 24 hours. Make sure the
    username is correct and enter your new password. The password
    should meet the password requirements explained below and must
    be 'safe' or 'very safe' according to the provided password
    strength meter.

    ![Iris: Reset your password](images/iris_login_reset_pw_06b.png)
    {: align="center"}

7.  A dialog box asks to confirm that you want to reset password
    with the new one..  Click 'OK'.

    ![Iris: Are you sure?](images/iris_login_reset_pw_07.png)
    {: align="center" style="border:1px solid black"}

8.  Another dialog box tells that if your password reset succeeded,
    you will receive a confirmation email.

    ![Iris: Email sent again](images/iris_login_reset_pw_08.png)
    {: align="center" style="border:1px solid black"}

If you still have a problem after trying that, you will need to
send an email to accounts@nersc.gov describing the steps you took and the 
problem you are having. The Account Support personnel will respond to you 
during normal NERSC business hours. 

## How To Change Your Password in Iris

All of NERSC's computational systems are managed by the LDAP protocol
and use the Iris password. Passwords cannot be changed directly on
the computational machines, but rather the Iris password itself
must be changed, following the steps below:

1.  Point your browser to [https://iris.nersc.gov](https://iris.nersc.gov)
    and log in. If you are already in Iris instead, click the Iris
    icon in the top left corner, which will make Iris display
    information about your NERSC account.

    ![Iris menu bars](../iris/images/iris_elvis_menubars.png)
    {: align="center" style="border:1px solid black"}

3.  Select the 'Profile' tab.
4.  Click on the 'Reset Password' box to the right of the 'Self-service
    User Info' section.

    ![Iris: Email sent again](images/iris_profile_reset_pw_01.png)
    {: align="center" style="border:1px solid black"}

5.  In the dialog window that appears, enter the current password,
    and your new password. Your password should meet the password
    requirements explained in the next section below, and must be
    'safe' or 'very safe' according to the provided password strength
    meter.

    ![Iris: Email sent again](images/iris_profile_reset_pw_02.png)
    {: align="center" style="border:1px solid black"}

6.  Click the 'Set Password' button at the bottom.
7.  You will be asked to confirm your intention to change the
    password. Click 'OK'.

    ![Iris: Email sent again](images/iris_profile_reset_pw_03.png)
    {: align="center" style="border:1px solid black"}

8.  A green banner will appear at the top of the page, indicating
    that your password has been successfully changed.

    ![Iris: Email sent again](images/iris_profile_reset_pw_04.png)
    {: align="center" style="border:1px solid black"}

Passwords must be changed under any one of the following circumstances:

*  Immediately after someone else has obtained your password (do *NOT*
   give your password to anyone else).
*  As soon as possible, but at least within one business day after a
   password has been compromised or after you suspect that a password
   has been compromised.
*  On direction from NERSC staff.

Your new password must adhere to NERSC's password requirements.

## Password Requirements

NIST (National Institute of Standards and Technology) has updated
their Digital Identity Guidelines in [Special Publication
800-63B](https://pages.nist.gov/800-63-3/sp800-63b.html). Based on
the guidelines and LBNL's new password standard, NERSC has updated
password policy to bring it in closer alignment with the guidelines.
The vast majority of NERSC users utilize a second factor in addition
to their password when logging into systems and web apps, and this
further justifies a change in password policy.

When users select their own passwords for use at NERSC, the
following requirements must be satisfied.

*   The password must register as either 'safe' or 'very safe' on
    a password strength meter that is provided.
*   The enforced minimum length for accounts with MFA enabled is 8 characters, 
    but in practice it may be difficult to select one that registers as 'safe' 
    on the meter with such a short password. If MFA is not enabled for your
    account the minimum password length is 14 characters.
*   There is no character complexity rule regarding inclusion of
    uppercase/lowercase letters, digits and special characters.
*   Passwords must be changed every year (365 days).
  
If you are struggling to come up with a good password, Iris can recommend one 
for you. Click on the 'Recommend a safe password' link beneath the 'New 
password' box in the 'Password reset' dialog window.

## Login Failures

Your login privileges will be disabled if you have ten login failures
while entering your password on a NERSC resource. You do not need
a new password in this situation. The login failures will be
automatically cleared after 5 minutes. No additional actions are
necessary.
